# michael martin
# 2/22/2022
# ncd.py - compute the nearest compression distance between 2 audio files

import os
import sys
import pyflac
from pydub import AudioSegment

# audio sampled file paths
sample1 = sys.argv[1]
sample2 = sys.argv[2]
concat_path = "data/concat.wav"

# import audio samples
s1 = pyflac.FileEncoder(sample1, compression_level=8, verify=True) # labeled deepfake sample
s2 = pyflac.FileEncoder(sample2, compression_level=8, verify=True) # labeled ground truth sample

# concatenate deep fake and ground truth for ncd
concat_files = AudioSegment.from_wav(sample1) + AudioSegment.from_wav(sample2) 
concat_files.export(concat_path, format="wav")
concat = pyflac.FileEncoder(concat_path, compression_level=8, verify=True) # concatenated audio

# compress audio samples
bc1 = len(s1.process())
bc2 = len(s2.process())
bcc = len(concat.process())

print("sample1 compressed:", bc1)
print("sample2 compressed:", bc2)
print("concat compressed:", bcc)

s = (bcc - min(bc1, bc2)) / max(bc1, bc2)
print("similarity: "+str(s))